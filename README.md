# Exam Description

You will be asked to create two smart contracts. We do not expect you to have any prior knowledge in Solidity or blockchain in general. We will provide you the best learning materials available for you to succeed.

**You are guaranteed to learn something from this interview process.** It's a win-win situation for both of us :)

## Learning materials

You can find the best possible Learning materials in the materials folder of this repo. Clone this repo and use them

**Important** : Do not waste time synchronizing the mainnet or Mist. These will not be needed for your exam (nor in real-life blockchain development)

## Exam description

Go through the first 4 modules. At the end of module 4 there are two problems to be solved SPLITTER and REMITTANCE.

Come up with their solution.

## Exam requirements

### Minimum Requirement:
* Present a repository with a truffle project and your Solidity contracts.
* The contracts should work with TestRPC

### Bonus Points awareded for:
* Creating JS unit-tests for the positive cases (testing the correct business logic and events)
* Creating JS unit-tests for the negative cases (testing for throws)

### Infinite amount Bonus Points awareded for:
* Deploying and running the contracts in Ropsten

God speed and Good Luck

The LimeLabs Team